package request_repository;

import java.io.IOException;
import java.util.ArrayList;

import common_utility_package.Excel_Data_Reader;

public class Post_request_repository extends Endpoint {
	public static String post_TC1_Request() throws IOException
	{
		ArrayList<String> excelData=Excel_Data_Reader.Read_Excel_Data("API_data.xlsx", "POSTAPI", "Post_Tc_2");
             String req_name=excelData.get(1);
             String req_job=excelData.get(2);
             
		String Requestbody="{\n"
				+ "    \"name\": \""+req_name+"\",\n"
				+ "    \"job\": \""+req_job+"\"\n"
				+ "}";
		return Requestbody;
	}
	
}
