package restAssuredTestNGFramework;
import io.restassured.RestAssured;
import static  io.restassured.RestAssured.given;

public class post_ref1 {

	public static void main(String[] args) {
		String BaseURI="https://reqres.in/";
		String Requestbody="{\\r\\n\"\r\n"
				+ "				+ \"    \\\"name\\\": \\\"morpheus\\\",\\r\\n\"\r\n"
				+ "				+ \"    \\\"job\\\": \\\"leader\\\"\\r\\n\"\r\n"
				+ "				+ \"}";
		RestAssured.baseURI=BaseURI;
		given().header("Content-Type","application/json").body(Requestbody).log().all()
		.when().post("api/users").then().log().all().extract().response().asString();
		
	}

}
